# Connecting PIC to USB using Raspberry PI serial cable

This simple circuit sends each 2s this text to serial
port (dump from Putty in Serial mode):
```
PIC10F206, Dumping ASCII table...
 !"#$%&'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\]^_`abcdefghijklmnopqrstuvwxyz{|}
```

![PIC to USB Serial PICDEM board](images/pic10f206-serial.jpg)

![PIC to USB Serial schematic](ExpressPCB/pic-usb-serial.png)

Architecture overview:

```
+-----------+      +-----------+      +------------+
|PC Putty   |      |USB Console|      |PICDEM board|
|Serial mode|----->|Cable #954 |----->|PIC10F206   |
|           |      |           |      |            |
+-----------+      +-----------+      +------------+
```


What you need:

* USB Console Cable #954 - this connects on one side to PC and provides
  TTL compatible serial ports signal to our PIC

  
    Example available on: https://www.adafruit.com/product/954

  
    > WARNING! My "USB Console Cable #954" does not work
    > with recent Prolific drivers. See chapter bellow for workaround 
  
  
* [DM163045 - PICDEM Lab Development Kit][DM163045], this kit includes also
  PicKit 3 Programmer/Debugger
* [PIC10F206][PIC10F206] - included with [DM163045 PICDEM kit][DM163045]
* Optional: [Elegoo 130 Jumper Wire Male Male]. Although PICDEM kit
  includes wires - their thickness is suitable rather for iron
  than TTL logic.
* MPLAB X IDE v2.35 (the least one that checks calibration value
  and allow overwrite of calibration, still fast and works under XP)

Tested OS: XP SP3 under VirtualBox 5.2.10 with extension pack (for USB 2.0 access)

I have following pass-through USB 2.0 devices in VirtualBox:

* `Microchip Technology Inc. PICkit 3 [0002]` - PicKit 3 programmer
* `Prolific Technology, Inc. PL2303 Serial Port [0300]` - USB Console Cable #954

## Oscillator calibration

This program requires precision clock source. Internal oscillator
frequency/4 must be 1MHz ±4% or better.
To verify that your PIC has proper clock source use this short program:
```asm
    LIST P=PIC10F206
    INCLUDE <P10F206.INC>

    __CONFIG  _MCLRE_ON & _CP_OFF & _WDT_OFF

RES_VECT  CODE    0x0000            ; processor reset vector
    IORLW   (1<<FOSC4) ; enable FOSC/4 (1MHz) output
    MOVWF   OSCCAL
    GOTO $             ; loop forever
```

And connect your frequency meter to to pin GP2/T0CKI/FOSC4.
There should be 1MHz ±1% calibrated from factory.

## Fixing driver for "USB Console Cable #954"

If you have poor luck (like me) you can have troubles with
your cable and recent Windows drivers.

My cable was labeled on e-shop
as `USB to TTL Serial Cable - Debug /Console Cable for Raspberry Pi`
Model `FFR-JMP` ordered from ModMyPi on 2013.
It is visible on archive page https://web.archive.org/web/20140425050649/https://www.modmypi.com/raspberry-pi-accessories
(right top corner). The bag containing cable is labeled `USB Console Cable #954`.

> Disclaimer: I have no clue which series are affected. I don't know
> whether current version available
> from https://www.modmypi.com/raspberry-pi/communication-1068/serial-1075/usb-to-ttl-serial-cable-debug--console-cable-for-raspberry-pi
> exhibits this problem or not.

The problem:

> Official driver does not work with this "flavor" of Prolific
> chip - there is an error `The device cannot start (Code 10)`
> in Windows Device Manager
> Fortunately fix available on:
> - http://www.totalcardiagnostics.com/support/Knowledgebase/Article/View/92/20/prolific-usb-to-serial-fix-official-solution-to-code-10-error

## Setup

If you have problematic `Prolific chip` then:

1. Download older driver from
   http://www.totalcardiagnostics.com/support/Knowledgebase/Article/View/92/20/prolific-usb-to-serial-fix-official-solution-to-code-10-error
   in case of XP I used following 32-bit driver from above page:
   http://www.totalcardiagnostics.com/files/PL-2303_Driver_Installer.exe

1. install downloaded driver

Then proceed with:

1. connect "USB Console Cable #954" to PICDEM board as shown on schematic 

1. connect "USB Console Cable #954" to PC

1. Power-up PICDEM board

1. Download and install Putty from:
   https://the.earth.li/~sgtatham/putty/0.70/w32/putty-0.70-installer.msi

1. Get name of your USB Serial COM port:
    - open "Device Manager" - for example run `devmgmt.msc` 
    - expand "Ports (COM & LPT)"
    - remember your `x` value from:
      `Prolific USB-To-Serial Comm port (COMx)` 
    
1. Configure Putty connection:
    - type: `Serial`
    - "Serial line:" `COMx` (see above how to get `x` value)
    - expand Connection -> Serial and ensure following settigns:
        - "Serial line to connect to:" `COMx` (see above hot to get `x` value)                                           
        - "Speed (baud):" `9600`
        - "Data bits:" `8`
        - "Stop bits:" `1`
        - "Parity:" `None`
        - "Flow control:" `None`

1. Open this serial connection in putty

1. You should see new text every 2 seconds
   
# Resources


* [PIC10F206 datasheet][PIC10F206] 
* [DM163045 - PICDEM Lab Development Kit][DM163045]
* [Elegoo 130 Jumper Wire Male Male]
* MPLAB X IDE
    - http://ww1.microchip.com/downloads/en/DeviceDoc/MPLABX-v2.35-windows-installer.exe
      from page: http://www.microchip.com/development-tools/pic-and-dspic-downloads-archive
* IRFD 9020 - power P-Channel MOSFET - tranzistor data-sheet including package layout:
    - https://www.vishay.com/docs/91137/sihfd902.pdf
* Fix for "Device cannot start (Code 10)" for USB cable with Prolific chip:
    - http://www.totalcardiagnostics.com/support/Knowledgebase/Article/View/92/20/prolific-usb-to-serial-fix-official-solution-to-code-10-error
* Putty download homepage:
    - https://www.chiark.greenend.org.uk/~sgtatham/putty/latest.html

[PIC10F206]: https://www.microchip.com/wwwproducts/en/PIC10F206 "PIC10F206 datasheet"
[DM163045]: http://www.microchip.com/Developmenttools/ProductDetails/DM163045 "PICDEM Lab Development Kit"
[Elegoo 130 Jumper Wire Male Male]: https://www.amazon.de/dp/B06XBHXWBD/ "Elegoo 130 Jumper Wire Male Male"     