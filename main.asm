;  PIC USB to serial connection for PIC10F206
; transmits 7-bit ASCII characters each 2 seconds
; to PC via "USB Console Cable #954", TXD connected to pin GP1
;

    LIST P=PIC10F206
    INCLUDE <P10F206.INC>

    __CONFIG  _MCLRE_ON & _CP_OFF & _WDT_OFF

    CONSTANT nLED = GP2 ; bit for LEDs BUSY/READY
    CONSTANT nTRIS_LED = TRISIO2 ; bit for TRIS instruction
; WARNING! GP1 is used for ISP programming - can have only light load!
    CONSTANT nTXD = GP1 ; bit for Serial transfer from PIC to PC
    CONSTANT nTRIS_TXD = TRISIO1 ; bit for TRIS instruction

; user data = RAM registers
    UDATA
rCNT1   RES 1
rCNT2   RES 1
rCNT3   RES 1
rCHAR   RES 1
rCHAR2  RES 1

; program start
RES_VECT  CODE    0x0000            ; processor reset vector
    MOVWF   OSCCAL
    CLRWDT      ; needed for PSA change even when WDT is disabled
    ; NOT_GPWU = DISABLE Wake-up on Pin Change bit (GP0, GP1, GP3)
    ; NOT_GPPU = 0 => Enable Weak Pull-ups bit (GP0, GP1, GP3)
    ;                 (attempt to hold nTXD in 1 = stop state on power up)
    ; T0CS     = 0 => Transition on internal instruction cycle clock, FOSC/4
    ; T0SE     = 0 => Increment on low-to-high transition on the T0CKI pin
    ; PSA      = 0 => Prescaler assigned to Timer0
    ; PS<2:0>  = 0x7 => Prescaler 1:256 Timer0
    MOVLW  (1<<NOT_GPWU)|0x7
    OPTION
    MOVLW ~(1<<CMPON)
    MOVWF CMCON0           ; Turn Off Comparator to enable Output on GP1

    ; try to prepare values in Latch
    MOVLW 1 << nTXD  ; try to hold nTXD high (stop state)
    MOVWF GPIO

    MOVLW ~(1<< nTRIS_TXD | 1<< nTRIS_LED )  ; GP2 & GP1 OUTPUT, other are Inputs
    TRIS GPIO

    ; ensure that nTXD is really high
    MOVLW 1 << nTXD  ; try to hold nTXD high (stop state)
    MOVWF GPIO

gTX_AGAIN
    BSF GPIO,nLED
    CALL cWAIT_2s
    BCF GPIO,nLED

gAGAIN
; dump greeting text
    CLRF rCNT3
gNextGreet
    CALL cGreetingChars
    CALL cTX_CHARw
    CALL cGreetingChars ; restore character in W
    INCF rCNT3,f
    IORLW 0  ; test Z
    BTFSS STATUS,Z
    GOTO gNextGreet
; dump ASCII characters
    MOVLW ' ' ;
    MOVWF rCHAR2
gINCLOOP
    MOVFW rCHAR2
    CALL cTX_CHARw
    INCF rCHAR2,f
    BTFSS rCHAR2,7
    GOTO gINCLOOP
    MOVLW .13
    CALL cTX_CHARw
    MOVLW .10
    CALL cTX_CHARw

; wait a bit
    GOTO gTX_AGAIN

; custom text, rCNT3 is offset
cGreetingChars
    MOVF rCNT3,w
    ADDWF PCL,f
    DT "PIC10F206, Dumping ASCII table...",.13,.10,0

; wait ~ 2s using Timer0
cWAIT_2s
    MOVLW .30 ; wait ~ 2s
    MOVWF rCNT2
g3
    CALL cTMR_WAIT
    DECFSZ rCNT2,f
    GOTO g3
    RETLW 0

; wait for timer0 overflow: 1 tick = 1us
; waiting for 256 * 256 = 65536 tick = 65536 us = 65.536ms
cTMR_WAIT
    CLRF TMR0
gTW1
    BTFSS TMR0,7
    GOTO gTW1
gTW2
    BTFSC TMR0,7
    GOTO gTW2
    RETLW 0

; transmits character in W to PC via Serial cable, stores to rCHAR
cTX_CHARw
    MOVWF rCHAR
; transmits character in rCHAR to PC via Serial Cable
cTX_CHAR
; start bit
    BCF GPIO,nTXD
    MOVLW .31
    CALL cBAUD_DELAYw
; send byte
    MOVLW 8 ; number of transferred bits
    MOVWF rCNT2
TX_BIT
; XXX here is fuzz 2us comparing 0 or 1 bit - but it should be ok.
    BTFSS rCHAR,0
    BCF GPIO,nTXD
    BTFSC rCHAR,0
    BSF GPIO,nTXD
    CALL BAUD_DELAY
    RRF  rCHAR,f
    DECFSZ rCNT2,f
    GOTO TX_BIT
; stop bit
    GOTO $+1    ; wait 2us
    NOP         ; wait 1us
    BSF GPIO,nTXD
    CALL BAUD_DELAY
; end of transmit
    RETLW   0

; delay for one bit in 9600 Baud mode
; needed uS = 1 000 000 / 9 600 = 104.16 ~= 104 uS
BAUD_DELAY
; one loop takes 3us so 104 / 3 = 34
; - 15 us => 34 - = .29
    MOVLW .30
cBAUD_DELAYw
    MOVWF rCNT1
; one loop takes 3us
BLOOP
    DECFSZ rCNT1,f
    GOTO BLOOP
    NOP
    RETLW 0

    END
