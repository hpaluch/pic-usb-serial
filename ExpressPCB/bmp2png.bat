:: convert schematic bmp files to png files
@echo off
setlocal

set im=c:\Program Files\ImageMagick-7.0.8-Q16\magick.exe

for %%i in (*.bmp) do (
  "%im%" convert %%i %%~ni.png || goto error
  del %%i
)


echo OK
exit /b 0

:error
echo Error occured - code %errorlevel
exit /b 1